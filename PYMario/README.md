# PyMarI-O
Inspired by SethBling's "MarI/O - Machine Learning for Video Games" video, I implemented the NEAT algorithm on Python using OpenAI Gym Retro and NEAT-python libraries. It tries to tackle the 1st level of the original Super Mario Bros.

![](play.gif)

Instructions to run 

```
pip install -r requirements.txt
python -m retro.import rom/SuperMarioBros-Nes.nes

####
Main Script:

python main.py -h  To see details 

To run type:
python main_prueba.py --config_file <configuration file> --generations <Number of Generations>
Example: python main_prueba.py --config_file configuration_2 --generations 100
Out: file with name: configuration_2_stats_gen_100

Visualization Scripts: 

-  python Plot_Data.py -h to see details 

python Plot_Data.py --stats_file <StatsFile>
Example: python Plot_Data.py --stats_file Config_Files/configuration_2_stats_gen_100
Out: Binder with name configuration_2_stats_gen_100_Plots and inside 4 plots consisting of fitness mean, max and species plots

-  python Comparing_Plots.py

python Comparing_Plots.py --generation <Number of Generations> --number_files <configuration indices to compare>
Example: python Comparing_Plots.py --generation 50 --number_files 1,2,3
Out: Binder with name Comparing_Config_1,2,3_gen_50_Plots and inside fitness mean and max comparison plots of these configurations

```
